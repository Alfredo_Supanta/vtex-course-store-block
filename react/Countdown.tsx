import React, { useState } from 'react';
import {TimeSplit} from './typings/global';
import {tick, getTwoDaysFromNow} from './utils/time';
import {useCssHandles} from 'vtex.css-handles';



// esta interface define los tipos de valores de los props
interface CountdownProps {
  targetDate: string
}

const CSS_HANDLES = ['countdown']

const DEFAULT_TARGET_DATE = getTwoDaysFromNow();



const Countdown: StorefrontFunctionComponent<CountdownProps> = ({targetDate = DEFAULT_TARGET_DATE}) => {
  
  
  const [timeRemaning, setTime]= useState<TimeSplit>({
    hours: '00',
    minutes: '00',
    seconds:'00'
  })
  //renderizado de titulo componente
  
  
  const handles = useCssHandles(CSS_HANDLES)

  tick(targetDate, setTime);

  return(
      <div className={`${handles.countdown} db tc`}>
        {`${timeRemaning.hours}:${timeRemaning.minutes}:${timeRemaning.seconds}`}
      </div>
  )
}

// Referencia al contenido del editor del sitio
Countdown.schema = {
  title: 'editor.countdown.title',
  description: 'editor.countdown.description',
  type: 'object',
  properties: {
    targetDate:{
      title: 'Final date',
      description: 'Final date used in the countdown',
      type: 'string',
      default:'null'
    }
  },
}

export default Countdown
